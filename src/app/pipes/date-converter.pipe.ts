import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "dateConverter"
})

export class DateConverter implements PipeTransform {

    transform(date: string): any {
        const dateText = new Date(date).toLocaleDateString("hu-HU", {
            hour: "numeric",
            hour12: false,
            minute: "numeric"
        });
        return dateText;
    }

}

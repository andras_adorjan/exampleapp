import { Component, OnInit, Input, Output } from "@angular/core";
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Component({
    templateUrl: "./email.component.html",
    styleUrls: ["./email.component.css"]
})
export class EmailComponent implements OnInit {

    private prefixUrl: string;
    emailForm: FormGroup;
    emails: Object;
    address: string;
    message: string;

    constructor(private formBuilder: FormBuilder,
                private http: HttpClient) {
        this.prefixUrl = environment.apiPrefixUrl;

        this.emailForm = formBuilder.group({
            address: new FormControl(null),
            message: new FormControl(null)
        })
    }
    
    // Submit emails form
    onSubmit(event: Event): void {
        event.preventDefault();

        // Send address and message to api
        this.http.post(`${environment.apiPrefixUrl}email`, this.emailForm.value).subscribe(response => {
            let success = response['success']

            // If save is successful, get mails from db
            if (success) {
                this.getEmails();
            }
        })
    }

    // Get emails from database
    getEmails(): void {
        this.http.get(`${environment.apiPrefixUrl}email`).subscribe(response => {
            this.emails = response;
        })
    }
    
    // Init
    ngOnInit(): void {
        this.getEmails();
    }
    
}
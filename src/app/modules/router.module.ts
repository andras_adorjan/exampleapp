import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
import { EmailComponent } from "../components/email/email.component";
import { DateConverter } from "../pipes/date-converter.pipe";
import { environment } from "../../environments/environment";
import { Browser } from "protractor";

const routes: Routes = [
    {
        path: "",
        component: EmailComponent
    }
];

@NgModule({
    declarations: [
        EmailComponent,
        DateConverter
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}

export const routedComponents = [
    EmailComponent
]

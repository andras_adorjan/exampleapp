"use strict";

let express = require('express');
let app = express.Router();

// GET emails
app.get('/email', function (req, res) {
    let collection = db.collection('email');

    collection.find({})
    .toArray(function (err, emails) {
        res.send(emails);
    })
})

// POST email
app.post('/email', function (req, res) {
    let collection = db.collection('email')
    
    collection.insert({ address: req.body.address, message: req.body.message, created: Date.now() }, function (err, result) {
        collection.find({ address: req.body.address }).toArray(function (err, data) {
            if (data.length > 0) {
                res.send(JSON.stringify({success: true}));
            } else {
                res.send(JSON.stringify({success: false}));
            }
        })
    })
})

module.exports = app;
# ExampleApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.10.

## Package installation

Run `npm install` in root directory. Navigate to `server` directory and run `npm install` again.

## Development server

Run `ng serve` for a dev server (root dir). Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Node server

Navigate to `server` directory. Run `node server.js`.
